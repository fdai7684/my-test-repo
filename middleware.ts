import { NextRequest, NextResponse } from 'next/server';

export async function middleware(request: NextRequest) {
  const sessionToken = request.cookies.get('sessionToken')?.value;
  const nextPath = request.nextUrl.pathname;
  const pathWithParam = request.nextUrl.searchParams.get('returnUrl');

  if (!sessionToken && nextPath !== '/login') {
    if (nextPath === '/login') {
      return;
    }

    if (nextPath === '/logout') {
      return NextResponse.redirect(new URL('/', request.url));
    }

    return NextResponse.redirect(new URL(`/login?returnUrl=${nextPath}`, request.url));
  } else if (sessionToken && nextPath.startsWith('/login')) {
    if (pathWithParam) {
      return NextResponse.redirect(new URL(pathWithParam as string, request.url));
    } else {
      return NextResponse.redirect(new URL('/dashboard', request.url));
    }
  }

  return NextResponse.next();
}

export const config = {
  matcher: ['/login', '/dashboard', '/logout']
};
