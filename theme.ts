import { extendTheme } from '@chakra-ui/react';

export const theme = extendTheme({
  fonts: {
    heading: 'Source Sans 3',
    body: 'Source Sans 3',
    certificate: 'Sora, sans-serif'
  },

  colors: {
    primary: '#4CAF50',
    secondary: '#1A73E9',
    sharpBlue: '#2E89FF',
    lightBlue: '#e7eff9',
    textGrey: '#616161',
    fadeText: '#B7B7B7'
  },

  components: {
    Button: {
      baseStyle: {
        cursor: 'pointer',
        borderRadius: 'hug'
      },
      variants: {
        primary: {
          backgroundColor: 'primary',
          color: 'white',
          borderColor: 'GrayText',

          _hover: {
            backgroundColor: 'secondary'
          }
        },
        secondary: {
          backgroundColor: 'secondary',
          color: 'white',
          borderColor: 'GrayText',
          _hover: {
            backgroundColor: 'primary'
          }
        },

        tertiary: {
          backgroundColor: 'transparent',
          color: 'GrayText',
          _hover: {
            backgroundColor: 'transparent',
            color: 'textGrey'
          }
        },
        simple: {
          backgroundColor: 'white',
          color: 'black',
          _hover: {
            backgroundColor: 'primary'
          }
        }
      },

      size: {
        md: {
          fontSize: 'md',
          px: 6,
          py: 3
        },
        lg: {
          fontSize: 'lg',
          px: 8,
          py: 4
        }
      },

      defaultProps: {
        variant: 'primary',
        size: 'md'
      }
    }
  },

  radii: {
    hug: '5px',
    popover: '20px'
  },

  styles: {
    global: {
      'html, body': {
        scrollBehavior: 'smooth'
      }
    }
  }
});
