import React, {
  useMemo,
  useContext,
  useEffect,
  useState,
  ReactNode,
  createContext,
  useCallback
} from 'react';
import { useToast } from '@chakra-ui/react';
import { useCookie } from 'react-use';
import { useMutation } from '@apollo/client';
import { LOGIN } from 'graphql/Auth';

interface AuthContextType {
  user: UserType;
  signin: (username: string, password: string) => Promise<string | unknown>;
}

interface UserType {
  username: string;
  objectId: string;
  userID: string;
}

const AuthContext = createContext<AuthContextType>({
  user: {
    username: '',
    objectId: '',
    userID: ''
  },
  signin: () => Promise.resolve('')
});

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const toast = useToast();
  const [user, setUser] = useState<UserType>({
    username: '',
    objectId: '',
    userID: ''
  });

  const [, updateSessionToken] = useCookie('sessionToken');
  const [, updateUserId] = useCookie('userId');

  const [loginFunction] = useMutation(LOGIN, {
    awaitRefetchQueries: true
  });

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const stringifiedUser = localStorage.getItem('user') ?? '';
      const parsedUser = stringifiedUser && JSON.parse(stringifiedUser);

      setUser(parsedUser);
    }
  }, []);

  const signin = useCallback(
    async (username: string, password: string) => {
      try {
        const {
          data: { logIn }
        } = await loginFunction({
          variables: { username, password }
        });

        const objectId = logIn?.viewer?.user?.objectId;
        const userID = logIn?.viewer?.user?.id;
        const sessionToken: string = logIn?.viewer?.sessionToken;

        updateSessionToken(sessionToken, {
          expires: new Date(Date.now() + 1000 * 60 * 60 * 24)
        });

        updateUserId(objectId, {
          expires: new Date(Date.now() + 1000 * 60 * 60 * 24)
        });

        setUser({
          username,
          objectId,
          userID
        });

        localStorage.setItem(
          'user',
          JSON.stringify({
            username,
            objectId,
            userID
          })
        );

        localStorage.setItem(
          'token',
          JSON.stringify({
            sessionToken
          })
        );

        toast({
          description: 'Erfolgreich eingeloggt',
          status: 'success',
          duration: 5000,
          isClosable: true
        });

        return 'success';
      } catch (error) {
        toast({
          description: error.message,
          status: 'error',
          duration: 5000,
          isClosable: true
        });
      }
    },
    [toast, loginFunction, updateSessionToken, updateUserId]
  );

  const value = useMemo(() => ({ user, setUser, signin }), [user, signin, setUser]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};
