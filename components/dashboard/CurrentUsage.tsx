import { useState } from 'react';
import {
  Text,
  VStack,
  HStack,
  IconButton,
  Stack
} from '@chakra-ui/react';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { LinkButton } from 'components/wrappers';

export const CurrentUsage = () => {
  const [showApiKey, setShowApiKey] = useState(false);

  const toggleApiKeyVisibility = () => {
    setShowApiKey(!showApiKey);
  };

  const apiKey = showApiKey
    ? '123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456123456'
    : '••••••';

  return (
    <Stack>
      <Stack gap={4}>
        <Text fontSize='2xl' fontWeight='bold'>
          Current Usage
        </Text>
        <Text fontSize='md' textColor='GrayText'>
          You have made 0 requests today. The free plan
          includes 500 requests each day, resetting at
          midnight UTC. Request statistics are updated every
          5 to 10 minutes.
        </Text>
      </Stack>
      <VStack alignItems='flex-start' pt={8} gap={6}>
        <Text fontSize='2xl' fontWeight='bold'>
          API Key
        </Text>
        <Text fontSize='md' textColor='GrayText'>
          Your API key is used to identify your application
          and must be included in all requests you make to
          RiskAPI.
        </Text>

        <HStack
          bgColor='lightBlue'
          alignItems='center'
          width='full'
          maxW='3xl'
          py={2}
          px={4}
          borderRadius='12px'
        >
          <Text
            flex={1}
            color='sharpBlue'
            wordBreak='break-all'
          >
            {apiKey}
          </Text>
          <IconButton
            aria-label={
              showApiKey ? 'Hide API Key' : 'Show API Key'
            }
            icon={
              showApiKey ? (
                <ViewOffIcon color='black' />
              ) : (
                <ViewIcon color='black' />
              )
            }
            background='transparent'
            border='none'
            isRound
            _hover={{ background: 'transparent' }}
            onClick={toggleApiKeyVisibility}
          />
        </HStack>
        <LinkButton
          href='/'
          variant='tertiary'
          border='1px solid GrayText'
          color='black'
          _hover={{
            bg: 'black',
            color: 'white'
          }}
        >
          Change API Key
        </LinkButton>
      </VStack>
    </Stack>
  );
};
