import {
  Text,
  Divider,
  Button,
  ListItem,
  SimpleGrid,
  Stack,
  ListIcon,
  List,
  Box
} from '@chakra-ui/react';
import { CustomBulletIcon } from 'icons/icons';
import useTranslation from 'next-translate/useTranslation';
import { plansData } from 'CONSTANTS/plans';
import { Link } from 'components/wrappers';

export const Plans = () => {
  const { t } = useTranslation('plans');

  return (
    <Stack spacing={6}>
      <Stack gap={4}>
        <Text fontSize='2xl' fontWeight='bold'>
          Plans
        </Text>
        <Text fontSize='md' textColor='GrayText'>
          Free for testing, option for commercial
        </Text>
      </Stack>
      <Stack flex={1}>
        <SimpleGrid width='full' minChildWidth='350px' gap={4}>
          {plansData.map(({ title, price, period, description, features }, index) => (
            <Stack key={index} p={8} borderRadius='lg' shadow='lg' gap={2}>
              <Text fontWeight='bold' fontSize='xl'>
                {t(title)}
              </Text>
              <Text fontSize='lg' fontWeight='bold' mt={2}>
                {t(price)}
                <Text fontSize='sm' as='span' color='gray.500' fontWeight='normal' ml={2}>
                  {t(period)}
                </Text>
              </Text>
              <Text color='gray.500'>{t(description)}</Text>
              <Divider height='2px' borderColor='textGrey' />
              <List py={4}>
                {features.map((feature, featureIndex) => (
                  <ListItem key={featureIndex} pb={2}>
                    <ListIcon as={CustomBulletIcon} />

                    {t(feature)}
                  </ListItem>
                ))}
              </List>

              <Button mt='auto' variant='secondary'>
                Start with {t(title)}
              </Button>
            </Stack>
          ))}
        </SimpleGrid>
        <Box fontSize='md' mt={8}>
          <Text>
            <Text as='span' fontWeight='bold' color='black' mr={2}>
              **
            </Text>
            <Link
              color='sharpBlue'
              fontWeight='bold'
              href='/dashboard?tab=2'
              textDecoration='underline'
              _hover={{ color: 'sharpBlue.600' }}
              mr={1}
            >
              {t('contactUs')}
            </Link>
            {t('partnerProgram')}
            <Text ml={1} as='span' fontWeight='bold' color='black' mr={1}>
              {t('ngos')}
            </Text>
            {t('and')}
            <Text ml={1} as='span' fontWeight='bold' color='black' mr={1}>
              {t('researchers')}
            </Text>
            {t('weValueYourWork')}
          </Text>
        </Box>
      </Stack>
    </Stack>
  );
};
