import { Text, useToast, Box, Stack } from '@chakra-ui/react';
import { CREATE_SUPPORT_MESSAGE_ENTRY } from 'graphql/SupportMessage';
import { useMutation } from '@apollo/client';
import { formValidator } from 'utls/formValidator';
import { object } from 'yup';
import { Form, Input, SubmitButton } from 'components/form-elements';
import { Textarea } from 'components/form-elements';
import useTranslation from 'next-translate/useTranslation';

interface SupportFormValues {
  email: string;
  message: string;
}
export const Proposal = () => {
  const { lang } = useTranslation();
  const language = lang as keyof typeof formValidator
  const validationSchema = object({
    email: formValidator[language].email,
    message: formValidator[language].string
  });
  const [createSupportMessage] = useMutation(CREATE_SUPPORT_MESSAGE_ENTRY);

  const toast = useToast();
  const onSubmit = async ({ email, message }: SupportFormValues) => {
    try {
      await createSupportMessage({
        variables: {
          email,
          message
        }
      });
      toast({
        title: 'Success!',
        description: 'You have been added to the waiting list.',
        status: 'success',
        duration: 9000,
        isClosable: true
      });
    } catch (error) {
      console.error(error);
      toast({
        title: 'An error occurred.',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true
      });
    }
  };
  return (
    <Stack spacing={6}>
      <Stack gap={4}>
        <Text fontSize='2xl' fontWeight='bold'>
          Proposal
        </Text>
        <Text fontSize='md' textColor='GrayText'>
          If you have any questions or suggestions regarding BWMA travel's services, please get in touch
          below. For non-urgent queries, you can also email at support@bwmaco.com
        </Text>
        <Text fontSize='md' textColor='GrayText'>
          To help us find a solution for your issue, please include as much detail as possible. This
          may include code samples and error logs. Since we do not log your text content, please
          also include a sample of your document where possible.
        </Text>
      </Stack>
      <Box width='full' p={8} bgColor='lightBlue' gap={4}>
        <Form
          onSubmit={onSubmit}
          initialValues={{
            email: '',
            message: ''
          }}
          validationSchema={validationSchema}
        >
          <Stack mx='auto'>
            <Text fontSize='md' textColor='GrayText'>
              Email
            </Text>
            <Input bgColor='white' name='email' />
            <Textarea name='message' label='Message' />
            <Box pt={3}>
              <SubmitButton variant='secondary'>Send</SubmitButton>
            </Box>
          </Stack>
        </Form>
      </Box>
    </Stack>
  );
};
