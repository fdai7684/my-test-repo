import {
  Stack,
  Text,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  HStack,
  useMediaQuery
} from '@chakra-ui/react';

import { Support, Proposal, RequestDemo } from './index';

export const ContactUs = () => {
  const tabData = [
    { title: 'RequestDemo', content: <RequestDemo /> },
    { title: 'Proposal', content: <Proposal /> },
    { title: 'Support', content: <Support /> }
  ];
  return (
    <Stack width='full'>
      <Tabs gap={6} orientation='horizontal' pl={6}>
        <TabList
          borderBottom='none'
          borderLeft='none'
          alignItems='center'
        >
          {tabData.map((tab, index) => (
            <Tab key={index} textAlign='left'>
              {tab.title}
            </Tab>
          ))}
        </TabList>
        <TabPanels>
          {tabData.map((tab, index) => (
            <TabPanel key={index} px={2}>
              {tab.content}
            </TabPanel>
          ))}
        </TabPanels>
      </Tabs>
    </Stack>
  );
};