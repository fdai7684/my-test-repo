import { useMediaQuery } from '@chakra-ui/react';

import React from 'react';

import { DesktopNav, MobileNav } from './Navigation';

export const Header = () => {
  const [isDesktop] = useMediaQuery('(min-width: 62em)');

  return isDesktop ? <DesktopNav /> : <MobileNav />;
};
