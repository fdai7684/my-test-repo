import {
  Box,
  Drawer,
  DrawerContent,
  DrawerOverlay,
  HStack,
  IconButton,
  Select,
  Stack,
  Image,
  useDisclosure,
} from "@chakra-ui/react";
import { Link, LinkButton } from "components/wrappers";
import { CLOSEICON } from "icons/icons";
import { HamburgerIcon } from "@chakra-ui/icons";
import { useRouter } from "next/router";
import setLanguage from "next-translate/setLanguage";
import { useAuth } from "context";
import useTranslation from "next-translate/useTranslation";

export const MobileNav = () => {
  const { t } = useTranslation("common");
  const { onClose, isOpen, onToggle } = useDisclosure();

  const { pathname, locale } = useRouter();
  const isWaitingList = pathname === "/waiting-list";
  const {
    user: { username },
  } = useAuth();

  return (
    <HStack
      as="header"
      px={4}
      py={6}
      bg={isWaitingList ? "sharpBlue" : "white"}
      gap={6}
      justify="space-between"
    >
      <Link
        href="/"
        color="black"
        _hover={{
          color: isWaitingList ? "black" : "secondary",
        }}
      >
          <Image
      src='/images/icons/logo.png'
      alt={t("emailSentAlt")}
      width='100px' // Adjust the width as needed
      height='auto' // Maintains the aspect ratio
    />
      </Link>

      <IconButton
        isRound
        size="lg"
        aria-label="Nav menu icon"
        background="transparent"
        color={isWaitingList ? "white" : "secondary"}
        onClick={onToggle}
        _hover={{ background: "transparent" }}
      >
        <HamburgerIcon w={6} h={6} />
      </IconButton>

      <Drawer placement="left" onClose={onClose} isOpen={isOpen} size="full">
        <DrawerOverlay />
        <DrawerContent px={8} py={5}>
          <Stack h="100%" gap={12}>
            <HStack justify="space-between" alignItems="center">
              <Image
                src="/images/icons/logo.png"
                alt={t("emailSentAlt")}
                width="100px" // Adjust the width as needed
                height="auto" // Maintains the aspect ratio
              />
              <CLOSEICON
                w={8}
                h={8}
                _hover={{ color: "secondary" }}
                onClick={onClose}
              />
            </HStack>

            <Stack gap={6}>
              <Link href="/" color="black" onClick={onClose}>
                {t("get-started")}
              </Link>
              <Link href="/pricing" color="black" onClick={onClose}>
                {t("pricing")}
              </Link>
              {username ? (
                <Link
                  href="/dashboard"
                  color={"black"}
                  _hover={{
                    color: "secondary",
                  }}
                >
                  Dashboard
                </Link>
              ) : null}
            </Stack>

            <Stack
              pb={{
                base: "env(safe-area-inset-bottom)", // For modern mobile browsers
                sm: "16px", // Fallback for older browsers
              }}
              mt="auto"
              align="center"
              gap={6}
            >
              <Box>
                <Select
                  border="1px solid"
                  borderColor="black"
                  color="black"
                  _hover={{
                    bg: "black",
                    color: "white",
                  }}
                  value={locale}
                  onChange={async (e) => await setLanguage(e.target.value)}
                >
                  <option value="de">DE</option>
                  <option value="en">EN</option>
                </Select>
              </Box>
              <LinkButton
                href={username ? "/dashboard" : "/waiting-list"}
                variant="secondary"
                color="white"
                background="secondary"
                _hover={{
                  bg: "primary",
                  color: "white",
                }}
                px={4}
              >
                {username ? username : t("getApiKey")}
              </LinkButton>

              <Link
                href={username ? "logout" : "login"}
                color="black"
                textAlign="center"
              >
                {username ? t("logOut") : t("logIn")}
              </Link>
            </Stack>
          </Stack>
        </DrawerContent>
      </Drawer>
    </HStack>
  );
};
