import { Text, Box, HStack, Select , Image} from '@chakra-ui/react';
import { Link, LinkButton } from 'components/wrappers';

import { useRouter } from 'next/router';
import setLanguage from 'next-translate/setLanguage';
import { useAuth } from 'context';
import useTranslation from 'next-translate/useTranslation';

export const DesktopNav = () => {
  const { t } = useTranslation('common');
  const { pathname, locale } = useRouter();
  const {
    user: { username }
  } = useAuth();
  const isWaitingList = pathname === '/waiting-list';

  const navLinks = [
    {
      text:  t("get-started"),
      href: '/'
    },
    {
      text:  t("pricing"),
      href: '/pricing'
    }
  ];

  return (
    <HStack as='header' px={20} py={6} bg={isWaitingList ? 'sharpBlue' : 'white'} gap={6}>
     
     <Image
      src='/images/icons/logo.png'
      alt={t("emailSentAlt")}
      width='100px' // Adjust the width as needed
      height='auto' // Maintains the aspect ratio
    />

      <HStack gap={4}>
        {navLinks.map(({ text, href }) => (
          <Link
            key={text}
            href={href}
            color={isWaitingList ? 'white' : 'black'}
            _hover={{
              color: isWaitingList ? 'black' : 'secondary'
            }}
          >
            {text}
          </Link>
        ))}
        {username ? (
          <Link
            href='/dashboard'
            color={isWaitingList ? 'white' : 'black'}
            _hover={{
              color: isWaitingList ? 'black' : 'secondary'
            }}
          >
           {t("dashboard")}
          </Link>
        ) : null}
      </HStack>

      <HStack gap={2} ml='auto'>
        <Box>
          <Select
            borderColor={isWaitingList ? 'white' : 'black'}
            color={isWaitingList ? 'white' : 'black'}
            _hover={{
              bg: isWaitingList ? 'white' : 'black',
              color: isWaitingList ? 'black' : 'white'
            }}
            value={locale}
            onChange={async e => await setLanguage(e.target.value)}
          >
            <option value='de'>DE</option>
            <option value='en'>EN</option>
          </Select>
        </Box>
        <LinkButton
          href={username ? '/logout' : '/login'}
          variant='tertiary'
          border='1px solid'
          borderColor={isWaitingList ? 'white' : 'black'}
          color={isWaitingList ? 'white' : 'black'}
        >
          {username ?  t("logOut"): t("logIn")}
        </LinkButton>

        <LinkButton
          href={username ? '/dashboard' : '/waiting-list'}
          variant='secondary'
          color={isWaitingList ? 'black' : 'white'}
          background={isWaitingList ? 'white' : 'secondary'}
        >
          {username ? username :  t("getApiKey")}
        </LinkButton>
      </HStack>
    </HStack>
  );
};
