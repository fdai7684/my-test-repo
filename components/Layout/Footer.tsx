import { Flex, Stack, Text, VStack, Image } from "@chakra-ui/react";
import { Link } from "components/wrappers";
import useTranslation from "next-translate/useTranslation";

export const Footer = () => {
  const { t } = useTranslation("common");
  const footerLinks = [
    {
      title: "BWMA & Co.",
      links: [
        {
          name: t("About"),
          href: "#",
        },
        {
          name: t("get-started"),
          href: "#",
        },
        {
          name: t("pricing"),
          href: "/pricing",
        },
      ],
    },
    {
      title: t("company"),
      links: [
        {
          name: t("privacy-policy"),
          href: "#",
        },
        {
          name: t("terms-of-use"),
          href: "/terms-of-use",
        },
      ],
    },
    {
      title: t("support"),
      links: [
        {
          name: t("contact-us"),
          href: "/dashboard?tab=2",
        },
      ],
    },
  ];

  return (
    <Stack
      as="footer"
      px={{ base: 4, md: 20 }}
      py={8}
      gap={6}
      align={{ base: "center", md: "start" }}
    >
      <Link href="/" color="black">
        {" "}
        <Image
          src="/images/icons/logo.png"
          alt={t("emailSentAlt")}
          width="100px" // Adjust the width as needed
          height="auto" // Maintains the aspect ratio
        />
      </Link>
      <Flex gap={16} direction={{ base: "column", md: "row" }}>
        {footerLinks.map((link) => (
          <VStack
            key={link.title}
            gap={6}
            align={{ base: "center", md: "start" }}
          >
            <Text m={{ base: "auto", md: "0", lg: "0" }} fontWeight="bold">
              {link.title}
            </Text>
            {link.links.map((item) => (
              <Link
                key={item.name}
                href={item.href}
                color="black"
                textAlign="center"
              >
                {item.name}
              </Link>
            ))}
          </VStack>
        ))}
      </Flex>
    </Stack>
  );
};
