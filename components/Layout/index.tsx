import { useRouter } from 'next/router';
import { ReactNode, useMemo } from 'react';
import { MinimalLayout } from './MinimalLayout/MinimalLayout';
import { LandingpageLayout } from './LandingpageLayout';

interface LayoutProps {
  children: ReactNode;
}

export const Layout = ({ children }: LayoutProps) => {
  const { pathname } = useRouter();

  const publicPathsNoLayout = useMemo(
    () => [
      '/login',
      '/signup',
      '/forgot-password',
      '/password-reset',
      '/password-reset/[code]',
      '/confirmation',
      '/confirmation/[code]'
    ],
    []
  );

  if (publicPathsNoLayout.includes(pathname)) {
    return <MinimalLayout>{children}</MinimalLayout>;
  }

  return <LandingpageLayout>{children}</LandingpageLayout>;
};

export default Layout;
