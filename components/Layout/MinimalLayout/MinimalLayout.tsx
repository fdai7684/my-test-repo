import { ReactNode } from 'react';
import { Header } from '../Header';
import { Footer } from '../Footer';

interface LayoutTypes {
  children: ReactNode;
}

export const MinimalLayout = ({
  children
}: LayoutTypes) => {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
};
