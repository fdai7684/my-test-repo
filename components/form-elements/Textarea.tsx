import { Textarea as ChakraTextarea, FormLabel, Text } from '@chakra-ui/react';
import { useField } from 'formik';
import { TextareaHTMLAttributes } from 'react';
import { FormControl } from './FormControl';

interface Props extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  name: string;
  label?: string | JSX.Element;
  isDisabled?: boolean;
  required?: boolean;
}

export const Textarea = ({ label, required, name, isDisabled, ...rest }: Props) => {
  const [{ value }, { touched }, { setValue, setTouched }] = useField(name);

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
    if (!touched) {
      setTouched(true);
    }
    setValue(e.target.value);
  };

  return (
    <FormControl name={name}>
      {label && (
        <FormLabel textColor='GrayText' mb={2}>
          {label}
        </FormLabel>
      )}
      <ChakraTextarea
        value={value}
        onChange={handleChange}
        focusBorderColor='secondary'
        variant='filled'
        bgColor='white'
        height='16rem'
        isRequired={required}
        isDisabled={isDisabled}
        {...rest}
      />
    </FormControl>
  );
};
