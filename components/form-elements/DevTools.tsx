import { Box } from '@chakra-ui/react';
import { useFormikContext } from 'formik';

interface Props {
  isDisabled?: boolean;
}

export const DevTools = ({ isDisabled }: Props) => {
  const {
    submitCount,
    dirty,
    initialStatus,
    isSubmitting,
    isValid,
    isValidating,
    status,
    touched,
    values,
    errors,
    validateOnBlur,
    validateOnChange,
    validateOnMount
  } = useFormikContext();

  if (isDisabled) {
    return null;
  }

  return (
    <Box as='pre' my={10} bgColor='gray.100'>
      {JSON.stringify(
        {
          submitCount,
          values,
          touched,
          errors,
          dirty,
          initialStatus,
          isSubmitting,
          isValid,
          isValidating,
          status,

          validateOnBlur,
          validateOnChange,
          validateOnMount
        },
        null,
        2
      )}
    </Box>
  );
};
