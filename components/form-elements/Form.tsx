import { Box } from '@chakra-ui/react';
import { ReactNode } from 'react';
import {
  Form as FormikForm,
  Formik,
  FormikConfig
} from 'formik';

interface Props extends FormikConfig<any> {
  children: ReactNode;
}

export const Form = ({ children, ...rest }: Props) => {
  return (
    <Box w='full' sx={{ '& > form': { w: 'full' } }}>
      <Formik {...rest}>
        <FormikForm>{children}</FormikForm>
      </Formik>
    </Box>
  );
};
