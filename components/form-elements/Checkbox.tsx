import { Checkbox as ChakraCheckbox } from '@chakra-ui/react';
import { useField } from 'formik';
import { InputHTMLAttributes } from 'react';
import { FormControl } from './FormControl';

interface Props
  extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label?: string | JSX.Element;
  isDisabled?: boolean;
  required?: boolean;
}

export const Checkbox = ({
  label,
  required,
  name,
  isDisabled
}: Props) => {
  const [{ value }, { touched }, { setValue, setTouched }] =
    useField(name);

  const handleChange = (
    e: React.FormEvent<HTMLInputElement>
  ): void => {
    if (!touched) {
      setTouched(true);
    }
    setValue(e.currentTarget.checked);
  };
  return (
    <FormControl name={name}>
      <ChakraCheckbox
        colorScheme='gray'
        isChecked={value}
        onChange={handleChange}
        isRequired={required}
        isDisabled={isDisabled}
      >
        {label}
      </ChakraCheckbox>
    </FormControl>
  );
};
