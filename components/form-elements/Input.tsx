import {
  FormLabel,
  Input as ChakraInput,
  InputProps
} from '@chakra-ui/react';
import { useField } from 'formik';
import { FormControl } from '.';

interface InputFieldProps extends InputProps {
  name: string;
  type?: string;
  label?: string;
  placeholder?: string;
}

export const Input = ({
  type,
  label,
  placeholder,
  name,
  ...rest
}: InputFieldProps) => {
  const [field] = useField(name);

  return (
    <FormControl name={name} position='relative'>
      <ChakraInput
        {...field}
        type={type}
        placeholder={placeholder}
        focusBorderColor='secondary'
        borderRadius='hug'
        variant='filled'
        width='100%'
        py={7}
        sx={{
          '&:focus + .label': {
            top: '0',
            transform: 'translateY(0) scale(0.8)',
            fontSize: 'md'
          }
        }}
        {...rest}
      />
      {label && (
        <FormLabel
          position='absolute'
          className='label'
          zIndex='1'
          backgroundColor='none'
          transition='all 0.3s'
          px={field?.value ? 2 : 4}
          top={field?.value ? '0' : '25%'}
          transform={
            field?.value
              ? 'translateY(0) scale(0.8)'
              : 'translateY(0)'
          }
          fontSize={field?.value ? 'md' : 'lg'}
        >
          {label}
        </FormLabel>
      )}
    </FormControl>
  );
};
