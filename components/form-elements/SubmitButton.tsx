import { Button, ButtonProps } from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import React, { PropsWithChildren } from 'react';
import { useStyleConfig } from '@chakra-ui/react';

export type NextChakraButtonProps =
  PropsWithChildren<ButtonProps>;

export const SubmitButton = ({
  variant,
  children,
  ...rest
}: NextChakraButtonProps) => {
  const { isSubmitting } = useFormikContext();
  const styles = useStyleConfig('Button', { variant });

  return (
    <Button
      __css={styles}
      type='submit'
      isLoading={isSubmitting}
      isDisabled={isSubmitting}
      {...rest}
    >
      {children}
    </Button>
  );
};
