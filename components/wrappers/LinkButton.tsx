import {
  useStyleConfig,
  ButtonProps as ChakraButtonProps
} from '@chakra-ui/react';

import { PropsWithChildren } from 'react';
import { Button } from './Button';
import Link from 'next/link';

export interface Props
  extends PropsWithChildren<ChakraButtonProps> {
  href: string;
  variant?: string;
  size?: string;
}

export const LinkButton = ({
  variant,
  href,
  children,
  size,
  ...chakraProps
}: Props) => {
  const styles = useStyleConfig('Button', {
    variant,
    size
  });

  return (
    <Link href={href} type='button' passHref>
      <Button __css={styles} {...chakraProps}>
        {children}
      </Button>
    </Link>
  );
};
