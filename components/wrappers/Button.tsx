import {
  Button as ChakraButton,
  ButtonProps as ChakraButtonProps,
  useStyleConfig
} from '@chakra-ui/react';
import React, { PropsWithChildren } from 'react';

export type NextChakraButtonProps =
  PropsWithChildren<ChakraButtonProps>;

export const Button = ({
  children,
  variant,
  ...chakraProps
}: NextChakraButtonProps) => {
  const styles = useStyleConfig('Button', { variant });

  return (
    <ChakraButton __css={styles} {...chakraProps}>
      {children}
    </ChakraButton>
  );
};
