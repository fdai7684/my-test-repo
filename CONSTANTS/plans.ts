export const plansData = [
  {
    title: 'firstPlan.title',
    description: 'firstPlan.description',
    price: 'firstPlan.price',
    period: 'firstPlan.period',
    features: [
      'firstPlan.features.feature1',
      'firstPlan.features.feature2',
      'firstPlan.features.feature3',
      'firstPlan.features.feature4'
    ]
  },
  {
    title: 'secondPlan.title',
    description: 'secondPlan.description',
    price: 'secondPlan.price',
    period: 'secondPlan.period',
    features: [
      'secondPlan.features.feature1',
      'secondPlan.features.feature2',
      'secondPlan.features.feature3',
      'secondPlan.features.feature4'
    ]
  },
  {
    title: 'thirdPlan.title',
    description: 'thirdPlan.description',
    price: 'thirdPlan.price',
    period: 'thirdPlan.period',
    features: [
      'thirdPlan.features.feature1',
      'thirdPlan.features.feature2',
      'thirdPlan.features.feature3',
      'thirdPlan.features.feature4'
    ]
  }
];
