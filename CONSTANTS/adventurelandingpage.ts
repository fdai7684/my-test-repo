// CONSTANTS/adventurelandingpage.js

// Example data for adventure features
export const ADVENTURE_FEATURES_DATA = [
    {
      heading: 'Skydiving Experience',
      text: 'Enjoy the thrill of freefall with our expert instructors.',
      imageLink: '/images/skydiving.jpg',
    },
    {
      heading: 'Mountain Skiing',
      text: 'Conquer the snowy slopes of majestic mountains with our skiing packages.',
      imageLink: '/images/skiing.jpg',
    },
    {
      heading: 'Scuba Diving Adventures',
      text: 'Explore the underwater world with our scuba diving excursions.',
      imageLink: '/images/scubadiving.jpg',
    },
    // Add more adventure features as needed
  ];
  
  // Example data for adventure section
  export const ADVENTURE_SECTION_DATA = [
    {
      text: 'Adventure Destinations',
      imageLink: '/images/adventure-destinations.jpg',
    },
    {
      text: 'Adrenaline Rush',
      imageLink: '/images/adrenaline-rush.jpg',
    },
    {
      text: 'Nature Exploration',
      imageLink: '/images/nature-exploration.jpg',
    },
    // Add more adventure section data as needed
  ];
  