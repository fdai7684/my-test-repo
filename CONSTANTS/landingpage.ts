export const FEATURES_DATA = [
  {
    imageLink: '/images/landing-page/types.png',
    text: 'features.feature1',
    heading: 'features-headings.text1'
  },
  {
    imageLink: '/images/landing-page/keyword.png',
    text: 'features.feature2',
    heading: 'features-headings.text2'
  },
  {
    imageLink: '/images/landing-page/location.png',
    text:  'features.feature3',
    heading: 'features-headings.text3'
  }
];

export const SECTION_DATA = [
  {
    imageLink: '/images/landing-page/001.png',
    text: 'section-texts.text1'
  },
  {
    imageLink: '/images/landing-page/002.png',
    text: 'section-texts.text2'
  },
  {
    imageLink: '/images/landing-page/003.png',
    text: 'section-texts.text3'
  }
];
