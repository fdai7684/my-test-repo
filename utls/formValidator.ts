import * as Yup from 'yup';

export const errorMessages = {
  de: {
    required: 'Bitte Pflichtfeld ausfüllen',
    requiredPackage: 'Bitte ein Paket hinzufügen',
    requiredMaxPackage: 'Maximal 5 Pakete',
    requiredSelect: 'Bitte Option auswählen',
    privacy: 'Zustimmung erforderlich',
    numb: 'Bitte eine gültige Nummer eingeben',
    phone: 'Bitte eine gültige Telefonnummer eingeben',
    zip: 'Bitte eine gültige Postleitzahl eingeben',
    email: 'Bitte eine gültige E-Mail-Adresse eingeben',
    confirmPassword: 'Das Passwort stimmt nicht überein',
    tags: 'Fehler?',
    empty: 'Kann nicht leer sein',
    lessthanchars: 'Das Passwort muss mindestens 8 Zeichen haben',
    file: 'Bitte ein Dokument hochladen',
  },
  en: {
    required: 'Please fill out this field',
    requiredPackage: 'Please add a package',
    requiredMaxPackage: 'Maximum 5 packages',
    requiredSelect: 'Please select an option',
    privacy: 'Consent required',
    numb: 'Please enter a valid number',
    phone: 'Please enter a valid phone number',
    zip: 'Please enter a valid postal code',
    email: 'Please enter a valid email address',
    confirmPassword: 'Password does not match',
    tags: 'Error?',
    empty: 'Cannot be empty',
    lessthanchars: 'Password must be at least 8 characters long',
    file: 'Please upload a document',
  },
};

export const formValidator = {
  de: {
    phonenumber: Yup.number().min(11, errorMessages.de.phone).required(errorMessages.de.phone),
    number: Yup.number().min(1, errorMessages.de.numb).required(errorMessages.de.numb),
    string: Yup.string().trim().min(1, errorMessages.de.required).required(errorMessages.de.required),
    select: Yup.mixed().required(errorMessages.de.requiredSelect),
    email: Yup.string().email(errorMessages.de.email).required(errorMessages.de.required),
    trueOnly: Yup.boolean().equals([true], errorMessages.de.privacy).required(errorMessages.de.required),
    password: Yup.string().min(8, errorMessages.de.lessthanchars).required(errorMessages.de.required),
    passwordConfirmation: Yup.string().test(
      'string',
      errorMessages.de.confirmPassword,
      function (value) {
        return this.parent.password === value;
      }
    ),
  },
  en: {
    phonenumber: Yup.number().min(11, errorMessages.en.phone).required(errorMessages.en.phone),
    number: Yup.number().min(1, errorMessages.en.numb).required(errorMessages.en.numb),
    string: Yup.string().trim().min(1, errorMessages.en.required).required(errorMessages.en.required),
    select: Yup.mixed().required(errorMessages.en.requiredSelect),
    email: Yup.string().email(errorMessages.en.email).required(errorMessages.en.required),
    trueOnly: Yup.boolean().equals([true], errorMessages.en.privacy).required(errorMessages.en.required),
    password: Yup.string().min(8, errorMessages.en.lessthanchars).required(errorMessages.en.required),
    passwordConfirmation: Yup.string().test(
      'string',
      errorMessages.en.confirmPassword,
      function (value) {
        return this.parent.password === value;
      }
    ),
  },
};
