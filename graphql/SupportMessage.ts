import { gql } from '@apollo/client';

export const CREATE_SUPPORT_MESSAGE_ENTRY = gql`
  mutation CreateSUPPORT_MESSAGE(
    $email: String!
    $message: String!
  ) {
    createSupportMessage(
      input: {
        fields: { email: $email, message: $message }
      }
    ) {
      supportMessage {
        email
      }
    }
  }
`;
