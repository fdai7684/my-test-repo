import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation login($username: String!, $password: String!) {
    logIn(
      input: { username: $username, password: $password }
    ) {
      viewer {
        sessionToken
        user {
          id
          objectId
          username
        }
      }
    }
  }
`;

export const SIGNUP = gql`
  mutation SignUp(
    $username: String!
    $email: String!
    $password: String!
  ) {
    register(
      input: {
          username: $username
          email: $email
          password: $password
      }
    ) {
     success
    }
  }
`;

export const LOGOUT = gql`
  mutation logout {
    logOut(input: { clientMutationId: "" }) {
      ok
    }
  }
`;
