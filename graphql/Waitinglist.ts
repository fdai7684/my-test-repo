import { gql } from '@apollo/client';

export const CREATE_WAITINGLIST_ENTRY = gql`
  mutation CreateWaitingList(
    $name: String!
    $email: String!
    $hasAgreedTerms: Boolean!
  ) {
    createWaitingList(
      input: {
        fields: {
          name: $name
          email: $email
          hasAgreedTerms: $hasAgreedTerms
        }
      }
    ) {
      waitingList {
        name
      }
    }
  }
`;
