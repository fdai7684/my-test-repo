import {
  Stack,
  Text,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  HStack,
  useMediaQuery
} from '@chakra-ui/react';

import { NextPage } from 'next';
import { CurrentUsage, ContactUs, Plans } from 'components/dashboard';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';

const Dashboard: NextPage = () => {
  const [isDesktop] = useMediaQuery('(min-width: 48em)');
  const { t } = useTranslation('dashboard');

  const tabData = [
    { title: t('tabs.currentUsage'), content: <CurrentUsage /> },
    { title: t('tabs.plan'), content: <Plans /> },
    { title: t('tabs.contactUs'), content: <ContactUs /> }
  ];
  const router = useRouter();
  const activeTab = Number(router.query.tab) || 0;

  const handleTabChange = (index: number) => {
    router.push(`/dashboard?tab=${index}`);
  };

  return (
    <HStack py={3} px={{ base: 4, md: 20 }}>
      <Stack width='full'>
        <Text>{t('accountSettings')}</Text>
        <Tabs
          onChange={handleTabChange}
          index={activeTab}
          gap={6}
          orientation={isDesktop ? 'vertical' : 'horizontal'}
          pl={6}
        >
          <TabList
            borderBottom='none'
            borderLeft='none'
            alignItems={isDesktop ? 'flex-start' : 'center'}
          >
            {tabData.map((tab, index) => (
              <Tab key={index} textAlign='left'>
                {tab.title}
              </Tab>
            ))}
          </TabList>
          <TabPanels>
            {tabData.map((tab, index) => (
              <TabPanel key={index} px={2}>
                {tab.content}
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </Stack>
    </HStack>
  );
};

export default Dashboard;
