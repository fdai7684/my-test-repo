import {
  HStack,
  Stack,
  Text,
  Image,
  UnorderedList,
  ListItem,
  Box
} from '@chakra-ui/react';
import { ArrowForwardIcon } from '@chakra-ui/icons';
import { NextPage } from 'next';
import {
  ADVENTURE_FEATURES_DATA,
  ADVENTURE_SECTION_DATA
} from 'CONSTANTS/adventurelandingpage'; // Assuming you have constants specific to adventure landing page
import { Link, LinkButton } from 'components/wrappers';
import useTranslation from 'next-translate/useTranslation';

const AdventureLandingPage: NextPage = () => {
  const { t } = useTranslation('home');

  return (
    <Stack>
      {/* Hero Section */}
      <Stack
        as='section'
        px={{ base: 4, md: 20 }}
        py={12}
        gap={6}
        align='center'
        justify='center'
      >
        {/* Hero Content */}
        <Stack maxW='4xl' alignItems='center'>
          <Text
            fontSize={{ base: '3xl', md: '4xl', lg: '5xl' }}
            color='fadeText'
            textAlign='center'
          >
         I am a trial part 2
            <Text as='span' fontWeight='bold' color='black'>
              {t('heading-adventure')}
            </Text>
          </Text>
          <Text
            color='textGrey'
            fontSize={{ base: 'sm', md: 'md' }}
            textAlign='center'
            pb={12}
          >
            {t('introduction')}
            <Link ml={1} href='/adventure-learn-more'>
              {t('learn-more')}
            </Link>
          </Text>

          {/* Call to Action Button */}
          <LinkButton
            href='/adventure-packages'
            variant='secondary'
            size='lg'
          >
            {t('explore-packages')}
            <ArrowForwardIcon ml={2} w={6} h={6} />
          </LinkButton>
        </Stack>
      </Stack>

      {/* Adventure Section */}
      <Stack
        as='section'
        px={{ base: 4, md: 12, lg: 24 }}
        pt={{ base: 12, md: 24, lg: 24 }}
        pb={{ base: 20, md: 24, lg: 36 }}
        background='lightBlue'
        align='center'
      >
        {/* Adventure Section Title */}
        <Text
          color='textGrey'
          fontSize={{ base: '2xl', md: '4xl' }}
          textAlign='center'
          pb={12}
        >
          {t('section-title')}
        </Text>

        {/* Adventure Features */}
        <HStack
          gap={12}
          flexWrap={{ base: 'wrap', md: 'nowrap' }}
        >
          {ADVENTURE_SECTION_DATA.map(
            ({ text, imageLink }, index) => (
              <Stack
                mx='auto'
                key={index}
                align='center'
                spacing={4}
              >
                <Image src={imageLink} alt='Adventure Icon' />
                <Text
                  color='textGrey'
                  textAlign='center'
                  fontSize='lg'
                >
                  {t(text)}
                </Text>
              </Stack>
            )
          )}
        </HStack>
      </Stack>

      {/* Adventure Features Section */}
      <Stack
        as='section'
        pt={{ base: 12, md: 24, lg: 24 }}
        pb={{ base: 20, md: 24, lg: 36 }}
      >
        {/* Adventure Features Title */}
        <Text
          fontWeight='bold'
          fontSize={{ base: '2xl', md: '3xl', lg: '4xl' }}
          textAlign='center'
        >
          {t('features-title')}
        </Text>

        {/* Adventure Features Details */}
        <Stack py={12} px={{ base: 12, md: 24 }} gap={12}>
          {ADVENTURE_FEATURES_DATA.map(
            ({ text, imageLink, heading }, index) => {
              const isEven = index % 2 === 0;
              return (
                <HStack
                  key={index}
                  flexWrap={{ base: 'wrap', md: 'nowrap' }}
                  flexDirection={
                    isEven ? 'row' : 'row-reverse'
                  }
                  justifyContent='center'
                  gap={24}
                >
                  {/* Adventure Feature Image */}
                  <Image src={imageLink} alt='Adventure Feature' />

                  {/* Adventure Feature Details */}
                  <Stack gap={12} maxWidth='xl'>
                    <Text fontWeight='bold' fontSize='xl'>
                      {t(heading)}
                    </Text>
                    <UnorderedList>
                      <ListItem color='textGrey'>
                        {t(text)}
                      </ListItem>
                    </UnorderedList>
                  </Stack>
                </HStack>
              );
            }
          )}
        </Stack>

        {/* Call to Action Button */}
        <Box mx='auto'>
          <LinkButton
            href='/adventure-packages'
            variant='secondary'
            size='lg'
          >
            {t('explore-packages')}
            <ArrowForwardIcon ml={2} w={6} h={6} />
          </LinkButton>
        </Box>
      </Stack>
    </Stack>
  );
};

export default AdventureLandingPage;
