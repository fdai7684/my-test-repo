import { useMutation } from '@apollo/client';
import { Spinner, Stack } from '@chakra-ui/react';
import { LOGOUT } from 'graphql/Auth';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { client } from 'pages/_app';
import { useCallback, useEffect } from 'react';
import { useCookie } from 'react-use';

const Logout: NextPage = () => {
  const { reload } = useRouter();
  const [, , deleteSessionToken] = useCookie('sessionToken');
  const [, , deleteUserId] = useCookie('userId');
  const [logout] = useMutation(LOGOUT);

  const handleLogout = useCallback(async () => {
    localStorage.clear();
    await logout();
    deleteSessionToken();
    deleteUserId();

    await client.resetStore();
    reload();
  }, [reload, deleteSessionToken, deleteUserId, logout]);

  useEffect(() => {
    handleLogout();
  }, [handleLogout]);

  return (
    <Stack width='100vw' height='100vh' align='center' justify='center' background='white'>
      <Spinner color='white' />
    </Stack>
  );
};

export default Logout;
