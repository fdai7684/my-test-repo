import { useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import {
  IconButton,
  InputGroup,
  InputRightElement,
  Stack,
  Text,
  useToast
} from '@chakra-ui/react';
import {
  Checkbox,
  Form,
  Input,
  SubmitButton
} from 'components/form-elements';
import { Link } from 'components/wrappers';
import type { NextPage } from 'next';
import { SIGNUP } from 'graphql/Auth';

import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { object } from 'yup';
import { formValidator } from 'utls/formValidator';
import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';



interface SignupFormValues {
  username: string;
  email: string;
  password: string;
  tncCheckBox: boolean;
}

const SignupPage: NextPage = () => {
  const { t, lang } = useTranslation("signup");
  const language = lang as keyof typeof formValidator
  const validationSchema = object({
    username: formValidator[language].string,
    email: formValidator[language].email,
    password: formValidator[language].password,
    confirmPassword: formValidator[language].passwordConfirmation,
    tncCheckBox: formValidator[language].trueOnly
  });
  const [showPassword, setShowPassword] = useState(false);
  const { push } = useRouter();
  const [signup] = useMutation(SIGNUP);
  const toast = useToast();

  const onSubmit = async ({
    username,
    email,
    password
  }: SignupFormValues) => {
    try {
      await signup({
        variables: {
          username,
          email,
          password
        }
      });
      push('/confirmation');
    } catch (error) {
      console.error(error);
      toast({
        title: t( "error-title"),
        description: t( "error-description"),
        status: 'error',
        duration: 9000,
        isClosable: true
      });
    }
  };

  return (
    <Stack py={24} px={4} align='center'>
      <Stack width='full' maxW='xl' gap={4}>
      <Text fontSize='xl' textAlign='center'>
          {t('create-account')}
        </Text>
        <Text color='grey' fontSize='sm'>
          {t('already-have-account')}
          <Link
            ml={2}
            href='/login'
            fontSize='sm'
            color='blue.500'
          >
            {t('sign-in')}
          </Link>
        </Text>

        <Form
          onSubmit={onSubmit}
          initialValues={{
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            tncCheckBox: false
          }}
          validationSchema={validationSchema}
        >
          <Stack gap={6}>
            <Input
              fontSize='sm'
              name='username'
              label={t('user-name')}
            />
            <Input
              label={t('email')}
              fontSize='sm'
              name='email'
              aria-label='Email Address'
            />

            <InputGroup
              sx={{
                '.login-eye': {
                  top: '3'
                }
              }}
            >
              <Input
                name='password'
                type={showPassword ? 'text' : 'password'}
                fontSize='sm'
                label={t('password')}
              />
              <InputRightElement
                className='login-eye'
                width='4rem'
                top='auto'
              >
                <IconButton
                  aria-label='show or hide password'
                  h='1.75rem'
                  background='transparent'
                  onClick={() =>
                    setShowPassword(!showPassword)
                  }
                  border='none'
                  isRound
                  _hover={{ background: 'transparent' }}
                >
                  {showPassword ? (
                    <ViewOffIcon color='black' />
                  ) : (
                    <ViewIcon color='black' />
                  )}
                </IconButton>
              </InputRightElement>
            </InputGroup>

            <Input
              fontSize='sm'
              name='confirmPassword'
              type="password"
              label={t('confirm-password')}
            />
            

            <Checkbox
              name='tncCheckBox'
              label={
                <Text
                  p={2}
                  textAlign='left'
                  color='grey'
                  fontSize='sm'
                >
                 {t( "read-agreed-tnc")}
                  <Link
                   ml={2}
                    href='/terms-of-use'
                    fontSize='sm'
                  >
                    {t("terms-and-conditions")}
                  </Link>
                </Text>
              }
            />

            <SubmitButton width='full' variant='secondary'>
             {t("sign-up")}
            </SubmitButton>
          </Stack>
        </Form>
      </Stack>
    </Stack>
  );
};

export default SignupPage;
