import { Text, Stack, useToast } from '@chakra-ui/react';
import { NextPage } from 'next';
import { Form, Input, SubmitButton } from 'components/form-elements';

import { object } from 'yup';
import { formValidator } from 'utls/formValidator';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';

interface PasswordResetFormValues {
  password: string;
}

const SETPASSWORD = gql`
  mutation passwordSet($code: String, $password: String) {
    passwordSet(input: { code: $code, password: $password }) {
      success
    }
  }
`;

const PasswordSetPage: NextPage = () => {
  const { t,lang } = useTranslation("password-reset");
  const language = lang as keyof typeof formValidator
  const validationSchema = object({
    password: formValidator[language].password,
    confirmPassword: formValidator[language].passwordConfirmation
  });
  const toast = useToast();
  const { push } = useRouter();
  const {
    query: { code }
  } = useRouter();
  const [setUserPassword] = useMutation(SETPASSWORD);

  const onSubmit = async ({ password }: PasswordResetFormValues) => {
    try {
      await setUserPassword({ variables: { code, password } });

      toast({
        description: t('success-message'), 
        status: 'success',
        duration: 3000,
        isClosable: true
      });
      push('/login');
    } catch (error) {
      toast({
        description: t('error-message'), 
        status: 'error',
        duration: 3000,
        isClosable: true
      });
    }
  };
  return (
    <Stack backgroundColor='white' align='center'>
      <Stack alignItems='center' justifyContent='center' pt={24} px={6}>
        <Text fontSize='xl' color='black' textAlign='center' pb={6}>
          {t('set-password')}
        </Text>
        <Form
          onSubmit={onSubmit}
          initialValues={{
            password: '',
            confirmPassword: ''
          }}
          validationSchema={validationSchema}
        >
          <Stack gap={6} align='center'>
            <Input name='password' type='password' label={t('password')} />

            <Input name='confirmPassword' type='password' label={t('confirm-password')} />
            <SubmitButton variant='secondary'>{t('reset')}</SubmitButton>
          </Stack>
        </Form>
      </Stack>
    </Stack>
  );
};

export default PasswordSetPage;
