import { Text, Image, Stack } from '@chakra-ui/react';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';

const PasswordResetPage: NextPage = () => {
  const { t } = useTranslation('password-reset'); 

  return (
    <Stack backgroundColor='white'>
      <Stack
        alignItems='center'
        justifyContent='center'
        pt={24}
        px={6}
      >
        <Text
          fontSize='xl'
          color='black'
          textAlign='center'
          pb={4}
        >
          {t('check-inbox')}
        </Text>
        <Image
          src='/images/icons/passwordResetEmailSent.png'
          alt='Email sent'
          pb={4}
        />
        <Text fontSize='sm' pb={4} textAlign='center'>
          {t('email-sent-text')}
        </Text>
      </Stack>
    </Stack>
  );
};

export default PasswordResetPage;
