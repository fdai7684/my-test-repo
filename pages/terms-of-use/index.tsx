import { Box, Text, Stack, VStack, Heading } from "@chakra-ui/react";
import { NextPage } from "next";
import useTranslation from "next-translate/useTranslation";

const TermsOfUsePage: NextPage = () => {
  const { t } = useTranslation("terms");

  return (
      <Stack
        m="auto"
        maxW={{ base: "100%", md: "70%" }}
        gap={4}
        pt={12}
        px={4}
      >
        <Text
          alignSelf="flex-start"
          fontSize={{ base: "md", md: "xl" }}
          color="textGrey"
        >
          {t("date")}
        </Text>
        <Heading
          alignSelf={{ base: "flex-start", md: "center" }}
          fontSize={{ base: "3xl", md: "4xl" }}
          color="black"
          wordBreak="break-word"
        >
          {t("title")}
        </Heading>
        <Heading
          alignSelf="flex-start"
          fontSize="xl"
          color="black"
          wordBreak="break-word"
        >
          {t("consent-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("consent-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("license-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("license-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("data-license-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("data-license-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("technical-errors-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("technical-errors-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("subscriptions-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("subscriptions-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("payment-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("payment-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("sla-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("sla-description-paragraph-1")}
        </Text>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("sla-description-paragraph-2")}
        </Text>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("sla-description-paragraph-3")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("customer-data-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("customer-data-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("copyright-infringement-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("copyright-infringement-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("disclaimer-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("disclaimer-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("indemnification-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("indemnification-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("changes-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("changes-description")}
        </Text>
        <Heading
          fontSize="xl"
          color="black"
          alignSelf="flex-start"
          wordBreak="break-word"
        >
          {t("governing-law-title")}
        </Heading>
        <Text
          fontSize="md"
          color="textGrey"
         alignSelf= "flex-start"
          wordBreak="break-word"
        >
          {t("governing-law-description")}
        </Text>
      </Stack>
  );
};

export default TermsOfUsePage;
