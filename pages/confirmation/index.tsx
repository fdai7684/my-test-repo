import { Text, Image, Stack } from '@chakra-ui/react';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';

const ConfirmationPage: NextPage = () => {
  const { t } = useTranslation("confirmation");
  return (
    <Stack
      gap={4}
      alignItems='center'
      justifyContent='center'
      py={36}
      px={6}
    >
      <Text fontSize='xl' color='black'>
       {t( "verifyEmail")}
      </Text>
      <Image
        src='/images/icons/emailVerified.png'
        alt={t("emailSentAlt")}
      />
      <Text textAlign='center' fontSize='xl'>
       {t(  "verificationMessage")}
      </Text>
    </Stack>
  );
};

export default ConfirmationPage;
