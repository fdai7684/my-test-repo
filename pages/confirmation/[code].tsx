import { useEffect, useState } from 'react';
import { Image, useToast, Spinner, Text, Stack } from '@chakra-ui/react';
import { LinkButton } from 'components/wrappers';
import { NextPage } from 'next';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation'; 

const CONFIRM = gql`
  mutation confirm($code: String) {
    confirm(input: { code: $code }) {
      success
    }
  }
`;

const EmailConfirmationPage: NextPage = () => {
  const { t } = useTranslation('confirmation'); 
  const toast = useToast();
  const [confirmCode, { loading }] = useMutation(CONFIRM);
  const {
    query: { code }
  } = useRouter();
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const confirmUser = async () => {
      try {
        const { data, errors } = await confirmCode({ variables: { code } });

        if (errors) {
          setErrorMessage(errors[0]?.message);
        }

        const success = data?.confirm?.success;

        if (success) {
          toast({
            description: t('verificationSuccess'), 
            status: 'success',
            duration: 3000,
            isClosable: true
          });
        } else {
          setErrorMessage(t('confirmationFailed')); 
        }
      } catch (error) {
        setErrorMessage(error.message);
        toast({
          description: error.message,
          status: 'error',
          duration: 3000,
          isClosable: true
        });
      } finally {
        setIsLoaded(true);
      }
    };

    if (code && window && !isLoaded) {
      confirmUser();
    }
  }, [code, confirmCode, toast, isLoaded, t]); 

  return (
    <Stack gap={4} alignItems='center' justifyContent='center' py={36} px={6}>
      {loading && !isLoaded ? (
        <Spinner />
      ) : errorMessage ? (
        <Text textAlign='center' fontSize='sm' pb={4}>
         {t("errorOccurred")}
        </Text>
      ) : isLoaded ? (
        <>
          <Text fontSize='xl' color='black' pb={4}>
            {t('emailVerified')} 
          </Text>

          <Image src='/images/icons/emailVerified.png' alt={t('emailSentAlt')} /> 

          <Text textAlign='center' fontSize='sm' pb={4}>
            {t('verificationSuccess')} 
          </Text>
          <LinkButton href='/login' variant='primary' size='lg'>
            {t('login')}
          </LinkButton>
        </>
      ) : null}
    </Stack>
  );
};

export default EmailConfirmationPage;
