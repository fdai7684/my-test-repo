import { ChakraProvider } from '@chakra-ui/react';
import Layout from 'components/Layout';
import 'focus-visible/dist/focus-visible';
import type { AppProps } from 'next/app';
import { theme } from '../theme';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  HttpLink,
  concat
} from '@apollo/client';
import { AuthProvider } from 'context';
const httpLink = new HttpLink({
  uri: process.env.NEXT_PUBLIC_SERVERNAME
});

const authMiddleware = new ApolloLink(
  (operation, forward) => {
    const token = localStorage.getItem('token');

    operation.setContext({
      headers: {
        'X-Parse-Application-Id':
          process.env.NEXT_PUBLIC_APPID,
        'X-Parse-Session-Token': token
          ? `${JSON.parse(token).sessionToken}`
          : ''
      }
    });
    return forward(operation);
  }
);

export const client = new ApolloClient({
  link: concat(authMiddleware, httpLink),
  cache: new InMemoryCache()
});

function ISH({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider resetCSS theme={theme}>
        <AuthProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </AuthProvider>
      </ChakraProvider>
    </ApolloProvider>
  );
}

export default ISH;
