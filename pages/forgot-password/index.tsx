import { Stack, Text } from '@chakra-ui/react';
import { Form, Input, SubmitButton } from 'components/form-elements';
import { Link } from 'components/wrappers';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useToast } from '@chakra-ui/react';
import { object } from 'yup';
import { formValidator } from 'utls/formValidator';
import { gql, useMutation } from '@apollo/client';
import useTranslation from 'next-translate/useTranslation';

interface ForgotPasswordFormValues {
  email: string;
}

const PasswordResetMutation = gql`
  mutation PasswordReset($email: String!) {
    passwordReset(input: { email: $email }) {
      success
    }
  }
`;

const ForgotpasswordPage: NextPage = () => {
  const { t,lang } = useTranslation("forgot-password");
  const language = lang as keyof typeof formValidator
  const validationSchema = object({
    email: formValidator[language].email
  });
  const { push } = useRouter();
  const [passwordresetreq] = useMutation(PasswordResetMutation);
  const toast = useToast();

  const onSubmit = async ({ email }: ForgotPasswordFormValues) => {
    try {
      await passwordresetreq({
        variables: {
          email
        }
      });
      toast({
        title: t( "success-title"),
        description: t( "success-description"),
        status: 'success',
        duration: 9000,
        isClosable: true
      });
      push('/password-reset');
    } catch (error) {
      console.error(error);
      toast({
        title: t( "error-title"),
        description: t( "error-description"),
        status: 'error',
        duration: 9000,
        isClosable: true
      });
    }
  };

  return (
    <Stack gap={4} alignItems='center' justifyContent='center' py={36} px={6}>
    <Stack maxWidth='3xl'>
      <Text fontSize='xl' color='black' textAlign='center' pb={4}>
        {t('enter-email')}
      </Text>
      <Form
        initialValues={{
          email: ''
        }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        <Stack gap={6} align='center'>
          <Input type='email' name='email' label={t('email')} />

          <SubmitButton type='submit' variant='secondary'>
            {t('send-email')}
          </SubmitButton>
        </Stack>
      </Form>
    </Stack>
  </Stack>
  );
};

export default ForgotpasswordPage;
