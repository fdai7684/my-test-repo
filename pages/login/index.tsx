import {
  IconButton,
  InputGroup,
  InputRightElement,
  Stack,
  Text
} from '@chakra-ui/react';
import {
  Form,
  Input,
  SubmitButton
} from 'components/form-elements';
import { Link } from 'components/wrappers';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useToast } from '@chakra-ui/react';
import { useAuth } from 'context';
import { useState } from 'react';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { object } from 'yup';
import { formValidator } from 'utls/formValidator';
import useTranslation from 'next-translate/useTranslation';



interface LoginFormValues {
  email: string;
  password: string;
}

const LoginPage: NextPage = () => {
  const { t, lang } = useTranslation('login');
  const language = lang as keyof typeof formValidator
  const validationSchema = object({
    email: formValidator[language].email,
    password: formValidator[language].password
  });
  const [showPassword, setShowPassword] = useState(false);
  const { reload } = useRouter();
  const { signin } = useAuth();

  const toast = useToast();
  const onSubmit = async ({
    email,
    password
  }: LoginFormValues) => {
    try {
      const response = await signin(email, password);
      if (response === undefined) {
        return;
      }

      reload();
    } catch (error) {
      console.error(error);
      toast({
        description: error.message,
        status: 'error',
        duration: 5000,
        isClosable: true
      });
    }
  };

  return (
    <Stack py={24} px={4} align='center'>
      <Stack width='full' maxW='xl' gap={4}>
        <Text fontSize='xl' textAlign='center'>
        {t( "welcome-back")}
        </Text>
        <Text color='grey' fontSize='sm'>
        {t('new-member-question')}{' '}
          <Link href='/signup' fontSize='sm'>
            {t('sign-up')}
          </Link>
        </Text>

        <Form
          onSubmit={onSubmit}
          initialValues={{
            email: '',
            password: ''
          }}
          validationSchema={validationSchema}
        >
          <Stack gap={6}>
            <Input name='email' label={t('email')} />

            <InputGroup
              sx={{
                '.login-eye': {
                  top: '3'
                }
              }}
            >
              <Input
                name='password'
                type={showPassword ? 'text' : 'password'}
                label={t('password')}
              />
              <InputRightElement
                className='login-eye'
                width='4rem'
                top='auto'
              >
                <IconButton
                  aria-label='show or hide password'
                  h='1.75rem'
                  background='transparent'
                  onClick={() =>
                    setShowPassword(!showPassword)
                  }
                  border='none'
                  isRound
                  _hover={{ background: 'transparent' }}
                >
                  {showPassword ? (
                    <ViewOffIcon color='black' />
                  ) : (
                    <ViewIcon color='black' />
                  )}
                </IconButton>
              </InputRightElement>
            </InputGroup>

            <Link
              href='/forgot-password'
              fontSize='sm'
              color='grey'
            >
             {t('forgot-password')}
            </Link>
            <SubmitButton width='full' variant='secondary'>
            {t('sign-in')}
            </SubmitButton>
          </Stack>
        </Form>
      </Stack>
    </Stack>
  );
};

export default LoginPage;
